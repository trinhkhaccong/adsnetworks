
import React, { useState, useEffect } from 'react';
import styles from '../../../../styles/Home.module.css'
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Menu } from 'antd';
export const UiMenu = () => {
    const [current, setCurrent] = useState(['news']);
    const [check, setCheck] = useState(false)
    const router = useRouter()
    const CheckMenu = async () => {
        await setCheck(false)
        let path = router.pathname.replace("/[moneyID]", "");
        path = path.replace("/[codeshopID]", "");
        path = path.replace("/[newsID]", "");
        if (path === "/") {
            path = "/news"
        }
        await setCurrent([path])
        await setCheck(true)
    }
    useEffect(() => {
        if (!router.isReady) return;
        CheckMenu()
    }, [router.isReady, router.asPath])

    return <div className='row'>
        {
            check && <Menu mode="horizontal" defaultSelectedKeys={current} theme="dark" style={{ fontWeight: "bold" }}>
                <Menu.Item key="/news">
                    <Link href={"/"}>Báo chí</Link>
                </Menu.Item>

                <Menu.Item key="/money" style={{ color: "#FFFFFF" }}>
                    <Link href={"/money"}>Làm nhiệm vụ kiếm tiền</Link>
                </Menu.Item>
                {/* <Menu.Item key="/code-shop" style={{color:"#FFFFFF"}}>
            <Link href={"/code-shop"}>Mã giảm giá</Link>
            </Menu.Item> */}
            </Menu>
        }

    </div>
};