import React from "react"
import styles from '../../../../styles/Home.module.css'
import { UiMenu } from "../UiMenu";
import { useRouter } from 'next/router';
import { Button, Dropdown, Menu } from 'antd';
import Link from "next/link";
import { useCookies } from "react-cookie";
import jwt_decode from "jwt-decode";

const menu = (cookies: any) => {
  let decoded:any = jwt_decode(cookies.token);
  if (decoded.is_admin == 1) {
    return (
      <Menu
        items={[
          {
            key: '1',
            label: (
              <Link href="/admin">
                <div className="btn btn-primary btn-sm col-12">Trang quản trị</div>
              </Link>
            ),
          },
          {
            key: '2',
            label: (
              <div className="btn btn-success btn-sm col-12">Số dư : {parseInt(cookies?.cash).toLocaleString('it-IT', { style: 'currency', currency: 'VND' })}</div>
            ),
          },
          {
            key: '3',
            label: (
              <Link href="/statistic">
                Lịch sử công việc
              </Link>
            ),
          }
          ,
          {
            key: '4',
            label: (
              <Link href="/cash">
                Thanh toán
              </Link>
            ),
          },
          {
            key: '4',
            label: (
              <Link href="/login">
                <div className="btn btn-danger btn-sm col-12">Đăng xuất</div>
              </Link>
            ),
          },
        ]}
      />
    )
  }
  else
  {
    return (
      <Menu
        items={[
          {
            key: '2',
            label: (
              <div className="btn btn-success btn-sm col-12">Số dư : {parseInt(cookies?.cash).toLocaleString('it-IT', { style: 'currency', currency: 'VND' })}</div>
            ),
          },
          {
            key: '3',
            label: (
              <Link href="/statistic">
                Lịch sử công việc
              </Link>
            ),
          }
          ,
          {
            key: '4',
            label: (
              <Link href="/cash">
                Thanh toán
              </Link>
            ),
          },
          {
            key: '4',
            label: (
              <Link href="/login">
                <div className="btn btn-danger btn-sm col-12">Đăng xuất</div>
              </Link>
            ),
          },
        ]}
      />
    )
  }
  
}

export const UiHead = () => {
  const router = useRouter()
  const [cookies, setCookie] = useCookies(['fullname', "token","cash"]);

  React.useEffect(() => {
    if (!router.isReady) return;
    // if (Object.keys(cookies).length === 0) router.push("/login")
  }, [ router.isReady]);
  return (
    <div className={styles.Uiheader}>
      <div className="d-flex justify-content-between">
        <div style={{ color: "orangered", fontSize: 18, fontWeight: "bold", paddingTop: 5 }}>
         <img src="https://i.postimg.cc/Z5BYXjK0/kiemtien.png"/>
        </div>
          <div className="col-6 menu-header"><UiMenu /></div>
        <div style={{ fontWeight: "bold", paddingTop: 5, paddingRight: 5 }}>
          {!(Object.keys(cookies).length === 0) && <Dropdown overlay={() => menu(cookies)} placement="bottom" arrow>
            <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {cookies?.fullname}
            </button>
          </Dropdown>}
          {
            Object.keys(cookies).length === 0 && <Link href={"/register"}>
              <button type="button" className="btn btn-success" aria-haspopup="true" aria-expanded="false">
                Đăng ký
              </button>
            </Link>
          }
          {" "}
          {
            Object.keys(cookies).length === 0 && <Link href={"/login"}>
              <button type="button" className="btn btn-primary" aria-haspopup="true" aria-expanded="false">
                Đăng nhập
              </button>
            </Link>
          }


        </div>
      </div>
    </div>
  )
}