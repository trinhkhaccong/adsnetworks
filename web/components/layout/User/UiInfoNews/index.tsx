import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'

export const UiInfoNews = (props: any) => {
    const { data } = props
    useEffect(() => {
    }, [data])
    return (
        <div className={styles.UiListNews}>
            <div className={styles.UiTitle}>{data.meta_title}</div>
            <div style={{ color: "#FFFFFF", padding: 5 }}>
                {data.meta_content}
            </div>

            <div style={{ color: "#FFFFFF" }}>
                {data.content?.split("\n").map((value: any, index: any) => {
                    return (
                        <div style={{ color: "#FFFFFF", padding: 5 }} key={data.meta_title}>
                            <br />
                            {
                                data?.list_img[index] && <div style={{textAlign:"center"}}><img className='col-sm-8' alt={data.meta_title} style={{ objectFit: "cover", cursor: "pointer" }} width='100%' src={data?.list_img[index]} />
                                    <br />
                                    <br />
                                </div>
                            }
                            {value}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
