import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import type { ColumnsType, TableProps } from 'antd/es/table';
import { UiCreateBank } from './UiCreateBank';
import { UiInfoBank } from './UiInfoBank';
import { UiHistoryWithdraw } from './UiHistoryWithdraw';
import { UiWithdraw } from './UiWithdraw';
import { Tabs } from 'antd';

export const UiCash = (props: any) => {
    const { data_info_bank,data_history_bank} = props
    const router = useRouter()
    useEffect(() => {
        if (!router.isReady) return;
    }, [props, router.isReady])

    return (
        <div>
            <div className='row'>
                <div className='col-md-6'>
                    <div className={styles.UiListNews}>
                        <div className="card-container m-1 mt-3">
                            <UiCreateBank />
                        </div>
                    </div>
                </div>
                <div className='col-md-6'>
                    <div className={styles.UiListNews}>
                        <div className="card-container m-1 mt-3">
                            <UiInfoBank data_share={data_info_bank} />
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.UiListNews}>
                        <div className="card-container m-1 mt-3">
                        <UiWithdraw data_share={data_info_bank} />
                        </div>
            </div>
            <div className={styles.UiListNews}>
                        <div className="card-container m-1 mt-3">
                            <UiHistoryWithdraw  data_share={data_history_bank}/>
                        </div>
            </div>
        </div>

    )
}