import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import { Select } from 'antd';
const { Option } = Select;

export const UiInfoBank = (props: any) => {
    const { data_share } = props
    const router = useRouter()
    useEffect(() => {
        if (!router.isReady) return;
    }, [data_share, router.isReady])

    return (

        <div className='pb-2'>
            <div className={styles.UiTitle}>Thông tin danh sách ngân hàng của bạn</div>
            {
                data_share?.map((value: any, index: any) => (
                    <div style={{ color: "orangered" ,background:"rgba(255, 255, 255, 0.1)",padding:5,borderRadius:10, fontWeight:"bold"}} className="mt-2">
                        <div className='d-flex justify-content-between'>
                            <div>{
                                value.bank_name
                            }
                            </div>
                            <div className='col-6'>
                                STK: {
                                    value.bank_number
                                }
                            </div>
                        </div>
                        <div className='d-flex justify-content-between'>
                            <div>Tên tài khoản: {
                                value.bank_user_name
                            }
                            </div>
                            <div className='col-6'>
                                Chi nhánh: {
                                    value.bank_address
                                }
                            </div>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}
