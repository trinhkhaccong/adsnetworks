import { useEffect, useState } from 'react'
import styles from '../../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import { Table } from 'antd';
import moment from 'moment';
import type { ColumnsType, TableProps } from 'antd/es/table';
import { APIPostInsertBank } from '../../../API';
import { Select } from 'antd';
import { useCookies } from 'react-cookie';
const { Option } = Select;

export const UiCreateBank = (props: any) => {
    const [cookie, setCookie] = useCookies(['phone'])
    const [bank_name, setBankName] = useState("")
    const [bank_user_name, setBankUser] = useState("")
    const [bank_number, setBankNumber] = useState("")
    const [bank_address, setBankAdderr] = useState("")
    const [msg, setMsg] = useState("")
    const router = useRouter()
    useEffect(() => {
        if (!router.isReady) return;
    }, [router.isReady])

    const handleChange = async (value: string) => {
        await setBankName(value)
    };
    const onSave = async () => {
        await setMsg("")
        let ret = await APIPostInsertBank({
            phone: cookie.phone,
            bank_name: bank_name,
            bank_user_name: bank_user_name,
            bank_number: bank_number,
            bank_address: bank_address
        })
        await setMsg(ret.messenger)
        if (ret.status == 1) {
            setTimeout(function () {
                window.open("/cash", "_parent")
            }, 1000);
        }
    }
    return (

        <div className='pb-2'>
            <div className={styles.UiTitle}>Tạo thông tin tài khoản rút tiền</div>
            <div style={{color:"#0070f3",padding:5}}>{msg}</div>
            <Select showSearch style={{ width: '100%' }} placeholder="Chọn thông tin ngân hàng" onChange={handleChange}>
                <Option key="BIDV">NGAN HANG TMCP DAU TU VA PHAT TRIEN VIET NAM (BIDV)</Option>
                <Option key="VIETINBANK">NGAN HANG TMCP CONG THUONG VIET NAM (VIETINBANK)</Option>
                <Option key="AGRIBANK">NGAN HANG NN VA PTNT VIETNAM (AGRIBANK)</Option>
                <Option key="SACOMBANK">NGAN HANG TMCP SAI GON THUONG TIN (SACOMBANK)</Option>
                <Option key="DONGABANK">NGAN HANG TMCP DONG A (DONGABANK)</Option>
                <Option key="VPBANK">NGAN HANG TMCP VIET NAM THINH VUONG (VPBANK)</Option>
                <Option key="TPBANK">NGAN HANG TMCP TIEN PHONG (TPBANK)</Option>
                <Option key="MB">NGAN HANG TMCP QUAN DOI (MB BANK)</Option>
                <Option key="EXIMBANK">NGAN HANG TMCP XUAT NHAP KHAU VIET NAM (EXIMBANK)</Option>
                <Option key="SEABANK">NGAN HANG TMCP DONG NAM A (SEABANK)</Option>
                <Option key="KIENLONGBANK">NGAN HANG TMCP KIEN LONG (KIENLONGBANK)</Option>
                <Option key="TECHCOMBANK">NGAN HANG TMCP KY THUONG VIET NAM (TECHCOMBANK)</Option>
                <Option key="ABBANK">NGAN HANG TMCP ANBINH (ABBANK)</Option>
                <Option key="SAIGONBANK">NGAN HANG TMCP SAI GON CONG THUONG (SAIGONBANK)</Option>
                <Option key="VIETBANK">NGAN HANG TMCP VIET NAM THUONG TIN (VIETBANK)</Option>
                <Option key="MSB">NGAN HANG TMCP HANG HAI VIET NAM (MSB)</Option>
                <Option key="CIMB">NGAN HANG TNHH MTV CIMB (CIMB)</Option>
                <Option key="VAB">NGAN HANG TMCP VIET A (VAB)</Option>
                <Option key="VIB">NGAN HANG TMCP QUOC TE VIB</Option>
                <Option key="SCB">NGAN HANG TMCP SAI GON (SCB)</Option>
                <Option key="CAKE">NGAN HANG TMCP VIET NAM THINH VUONG - NH SO CAKE BY VPBANK (CAKE)</Option>
                <Option key="IBK">NGAN HANG CONG NGHIEP HAN QUOC (IBK)</Option>
                <Option key="VRB">NGAN HANG LIEN DOANH VIET - NGA (VRB)</Option>
                <Option key="NASB">NGAN HANG TMCP BAC A (NASB)</Option>
                <Option key="VIETCAPITAL">NGAN HANG TMCP BAN VIET (VIETCAPITAL BANK)</Option>
                <Option key="BVB">NGAN HANG TMCP BAO VIET (BVB)</Option>
                <Option key="LPB">NGAN HANG TMCP BUU DIEN LIEN VIET (LPB)</Option>
                <Option key="PVCOMBANK">NGAN HANG TMCP DAI CHUNG VIET NAM (PVCOMBANK)</Option>
                <Option key="OCEANBANK">NGAN HANG TMCP DAI DUONG (OCEANBANK)</Option>
                <Option key="GPB">NGAN HANG TMCP DAU KHI TOAN CAU (GPB)</Option>
                <Option key="NAMABANK">NGAN HANG TMCP NAM A (NAMABANK)</Option>
                <Option key="HDB">NGAN HANG TMCP PHAT TRIEN TP.HCM (HDB)</Option>
                <Option key="OCB">NGAN HANG TMCP PHUONG DONG (OCB)</Option>
                <Option key="MHB">NGAN HANG TMCP PT NHA DONG BANG SONG CUU LONG (MHB)</Option>
                <Option key="NCB">NGAN HANG TMCP QUOC DAN (NCB)</Option>
                <Option key="ACB">NGAN HANG TMCP A CHAU (ACB)</Option>
                <Option key="SHINHANBANK">NGAN HANG TNHH SHINHAN VIET NAM (SHINHANBANK)</Option>
                <Option key="SHB">NGAN HANG TMCP SAI GON HA NOI (SHB)</Option>
                <Option key="BAOVIETBANK">NGAN HANG TMCP BAO VIET (BAOVIETBANK)</Option>
                <Option key="GPBANK">NGAN HANG TMCP DAU KHI TOAN CAU (GPBANK)</Option>
                <Option key="HDBANK">NGAN HANG TMCP PHAT TRIEN TP.HCM (HDBANK)</Option>
                <Option key="VIETABANK">NGAN HANG TMCP VIET A (VIETABANK)</Option>
            </Select>
            <div>
                <div style={{ color: "#FFF" }}>
                    Tên tài khoản
                </div>
                <input className='col-12' placeholder='VD : NGUYEN VAN A' onChange={(e) => setBankUser(e.target.value)} />
            </div>
            <div>
                <div style={{ color: "#FFF" }}>
                    Số tài khoản
                </div>
                <input className='col-12' placeholder='VD: 0123654895352' onChange={(e) => setBankNumber(e.target.value)} />
            </div>
            <div>
                <div style={{ color: "#FFF" }}>
                    Chi Nhánh
                </div>
                <input className='col-12' placeholder='VD: Hồ Chí Minh' onChange={(e) => setBankAdderr(e.target.value)} />
            </div>
            <div className='d-flex mt-2'>
                <div className='col'></div>
                <button type="button" className="btn btn-primary" onClick={onSave}>Lưu tài khoản</button>
            </div>
        </div>
    )
}
