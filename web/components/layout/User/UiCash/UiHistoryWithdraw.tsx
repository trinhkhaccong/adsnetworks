import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import moment from 'moment';
const Cash=(status:any)=>{
    if(status==='wait')
    {
        return <button type="button" className="btn btn-warning btn-sm">Đợi duyệt</button>
    }
    if(status==='success')
    {
        return <button type="button" className="btn btn-primary btn-sm">Thành Công</button>
    }
    if(status==='exit')
    {
        return <button type="button" className="btn btn-danger btn-sm">Bị hủy</button>
    }
}
export const UiHistoryWithdraw = (props: any) => {
    const { data_share } = props
    const router = useRouter()
    useEffect(() => {
        if (!router.isReady) return;
    }, [data_share, router.isReady])

    return (

        <div className='pb-2'>
        <div className={styles.UiTitle}>Lịch sử rút tiền</div>
        {
                data_share?.map((value: any, index: any) => (
                    <div style={{ color: "#FFFFFF" ,background:"rgba(255, 255, 255, 0.1)",padding:5,borderRadius:10, fontWeight:"400"}} className="mt-2">
                        <div className='row justify-content-between'>
                            <div className='col'>{
                                value.bank_name
                            }
                            </div>
                            <div className='col'>
                                STK : {
                                    value.bank_number
                                }
                            </div>
                            <div className='col'>
                            </div>
                        </div>
                        <div className='row'>
                        <div className='col'>
                            {
                                parseInt(value.cash)?.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})
                            }
                            </div>
                            <div className='col'>
                                {
                                    moment(value.date_time?.replace("T"," ")).format("DD/MM/YYYY ") +value.date_time?.slice(11,19)
                                }
                            </div>
                            <div className='col'>
                                Trạng thái: {Cash(value.status)}
                            </div>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}
