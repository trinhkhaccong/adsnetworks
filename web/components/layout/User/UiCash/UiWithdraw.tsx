import { useEffect, useState } from 'react'
import styles from '../../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import { Popover, Select } from 'antd';
import { useCookies } from 'react-cookie';
import { APIPostWithdraw } from '../../../API';
const { Option } = Select;

export const UiWithdraw = (props: any) => {
    const { data_share } = props
    const [cookie, setCookie] = useCookies(['phone'])
    const [cash,setCash] = useState("")
    const [bank,setBank] = useState("")
    const [check,setCheck] = useState(false)
    const [msg,setMsg] = useState("")
    const router = useRouter()
    useEffect(() => {
        if (!router.isReady) return;
    }, [data_share, router.isReady])

    const handleChange = async(value: string) => {
        await setBank(value)
    };
    const onSave=async()=>{
        await setMsg("")
        let data_bank = bank.split(":")
        let param:any={
            phone:cookie.phone,
            cash:cash,
            bank_name:data_bank[0],
            bank_number:data_bank[1]
        }
        let ret = await APIPostWithdraw(param)
        await setMsg(ret.messenger)
        if(ret.status ==1)
        {
            window.open("/cash","_parent")
        }
    }
    const CheckNumber=async(value:any)=>{
        if(value < 1000001 || value ==="")
        {
            await setCash(value)
            await setCheck(false)
        }
        else
        {
            await setCheck(true)
        }

    }
    return (

        <div>
            <div className={styles.UiTitle}>Yêu cầu rút tiền</div>
            <div style={{color:"#0070f3",padding:5}}>{msg}</div>
            <div className='row pb-2'>
                <div className='col-md-6'>
                    <Select style={{ width: '100%' }} placeholder="Chọn thông tin ngân hàng" onChange={handleChange}>
                        {
                            data_share?.map((value: any) => (
                                <Option key={value?.bank_name+":"+value?.bank_number}>{value?.bank_name} - {value?.bank_user_name}</Option>
                            ))
                        }
                    </Select>
                </div>
                <div className='col-md-6'>
                        <div className='d-flex justify-content-between'>
                            <Popover title="Vượt quá số tiền của bạn" open={check}>
                                <input type="number" className='col' placeholder='Nhập số tiền cần rút' onChange={(e)=>CheckNumber(e.target.value)}/>
                            </Popover>
                            <button className='btn btn-primary btn-sm' onClick={onSave} disabled={check}> Rút tiền</button>
                        </div>
                </div>
            </div>
        </div>
    )
}
