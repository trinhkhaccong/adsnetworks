import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'
import { useRouter } from 'next/router';

export const UiTrend =(props:any)=>{
    const {data} = props
    const router = useRouter()  
    useEffect(() => {

    }, [props])
    const onRouter=(value:any)=>{
        router.push("/news/"+value)
    }
    return (
        <div className={styles.UiListNews}>
            <div className={styles.UiTitle}>Bài viết được quan tâm nhiều</div>
            <div style={{color:"#FFFFFF",padding:5}}>
            {
                    data?.map((value: any) => {
                            return (
                                    <div style={{ color: "#FFFFFF", paddingBottom: 20, cursor: "pointer" }} onClick={()=>onRouter(value.link)} key={value.link}>
                                        <div style={{ color: "orangered", fontWeight: 'bold', paddingBottom: 10, fontSize: 16 }}>{value.meta_title}</div>
                                        <div className='row'>
                                            <img alt={value.meta_title} className={'col-md-4'} style={{ objectFit: "cover", cursor: "pointer" }} width='100%' src={value.meta_img} />
                                            <div className='col-md'>
                                                <div>Thời gian đăng bài : {value.date.replace("T", " ")}</div>
                                                <div style={{ paddingTop: 10 }}>{value.meta_content}</div>
                                            </div>
                                        </div>
                                    </div>
                                    
                            )
                    }
                    )
                }
            </div>
        </div>
    )
}
