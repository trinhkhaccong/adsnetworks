import React, { useState, useEffect } from 'react';
import styles from '../../../../styles/Home.module.css'
import Link from 'next/link';
import { useRouter } from 'next/router';
import { AndroidOutlined, AppleOutlined, MoneyCollectOutlined,QrcodeOutlined} from '@ant-design/icons';

export const UiMenuMoney = () => {
    const [current, setCurrent] = useState<any>('/money/ios');
    const [check,setCheck] = useState(false)
    const router = useRouter()
    const CheckMenu = async () => {
        await setCheck(false)
        let path = router.query.moneyID
        if(path===undefined)
        {
            path='ios'
        }
        await setCurrent(path)
        await setCheck(true)
    }
    useEffect(() => {
        if (!router.isReady) return;
        CheckMenu()
    }, [router.isReady, router.query.moneyID])

    return (
        <div>
            {
                check && <div className={styles.UiKeyWord}>
                <div className={styles.UiTitle}>Nhiệm vụ cài app</div>
                <Link href="/money/ios"><div className='col mt-2'><div className={current === 'ios' ? styles.SelectUiMenuMoney : styles.UiMenuMoney} onClick={async () => await setCurrent("ios")}>
                    <AppleOutlined /><div>IOS (Apple, Iphone, ...)</div>
                </div></div></Link>
                <Link href="/money/android"><div className='col mt-2'><div className={current === 'android' ? styles.SelectUiMenuMoney : styles.UiMenuMoney} onClick={async () => await setCurrent("android")}>
                    <AndroidOutlined /><div>ANDROID (Samsung...)</div>
                </div></div></Link>
            </div>
            }
            
            {/* <div className={styles.UiKeyWord}>
                <div className={styles.UiTitle}>Săn mã sàn thương mại điện tử</div>
                <Link href="/money/shoppe"><div className='col mt-2'><div className={current === 'cash' ? styles.SelectUiMenuMoney : styles.UiMenuMoney} onClick={async () => await setCurrent("cash")}>
                    <QrcodeOutlined /><div>Săn mã giảm giá Shoppe</div>
                </div></div></Link>
                <Link href="/money/tiki"><div className='col mt-2'><div className={current === 'cash' ? styles.SelectUiMenuMoney : styles.UiMenuMoney} onClick={async () => await setCurrent("cash")}>
                    <QrcodeOutlined /><div>Săn mã giảm giá Tiki</div>
                </div></div></Link>
                <Link href="/money/sendo"><div className='col mt-2'><div className={current === 'cash' ? styles.SelectUiMenuMoney : styles.UiMenuMoney} onClick={async () => await setCurrent("cash")}>
                    <QrcodeOutlined /><div>Săn mã giảm giá Sendo</div>
                </div></div></Link>
            </div> */}

            <div className={styles.UiKeyWord}>
                <div className={styles.UiTitle}>Dịch vụ tài chính</div>
                <Link href="/money/cash"><div className='col mt-2'><div className={current === 'cash' ? styles.SelectUiMenuMoney : styles.UiMenuMoney} onClick={async () => await setCurrent("cash")}>
                    <MoneyCollectOutlined /><div>Đăng ký thông tin - miễn lãi 7 ngày</div>
                </div></div></Link>
            </div>
        </div>
    )
}