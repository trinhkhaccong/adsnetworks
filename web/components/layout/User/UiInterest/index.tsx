import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'
import { UiNews } from "../UiNews"
import Link from 'next/link'
import { useRouter } from 'next/router';

export const UiInterest = (props: any) => {
    const { data } = props
    const router = useRouter()  

    useEffect(() => {

    }, [props])

    const onRouter=(value:any)=>{
        router.push("/news/"+value)
    }
    return (
        <div className={styles.UiListNews} style={{marginTop:20}}>
            <div className={styles.UiTitle}>Có thể bạn quan tâm </div>
            <div>
                {
                    data?.map((value: any, index: any) => {
                        if (index % 2 == 0) {
                            return (
                                <div className="row  justify-content-between mt-3" key={index}>
                                    <div style={{ color: "#FFFFFF", padding: 10, cursor: "pointer" }} className='col-lg-6' onClick={()=>onRouter(value.link)}>
                                        <div className='row'>
                                            <img alt={value.meta_title} className={'col-md-4'} style={{ objectFit: "cover", cursor: "pointer" }} width='100%' src={value.meta_img} />
                                            <div className='col-md'>
                                                <div style={{ color: "orangered", fontWeight: 'bold', paddingBottom: 10, fontSize: 16 }}>{value.meta_title}</div>
                                                <div>Thời gian đăng bài : {value.date.replace("T", " ")}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{ color: "#FFFFFF", padding: 10, cursor: "pointer" }} className='col-lg-6' onClick={()=>onRouter(data[index + 1]?.link)}>
                                        <div className='row'>
                                            <img alt={data[index + 1]?.meta_title} className='col-md-4' style={{ objectFit: "cover", cursor: "pointer" }} width='100%' src={data[index + 1]?.meta_img} />
                                            <div className='col-md'>
                                                <div style={{ color: "orangered", fontWeight: 'bold', paddingBottom: 10, cursor: "pointer", fontSize: 16 }}>{data[index + 1]?.meta_title}</div>
                                                <div >Thời gian đăng bài : {data[index + 1]?.date.replace("T", " ")}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            )
                        }
                    }
                    )
                }
            </div>
        </div>
    )
}