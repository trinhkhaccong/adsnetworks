import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'
import { UiNews } from "../UiNews"
import Link from 'next/link'
import { useRouter } from 'next/router';
import { Button, Modal, Space } from 'antd';
import { useCookies } from 'react-cookie';
import { detect } from 'detect-browser';
import {APIPostShareLink,APIGetCash} from "../../../../components/API"
const Status=(value:any)=>{
    if(value==true)
        {
            return <button className='btn btn-success btn-sm'>Trạng thái : triển khai</button>
        }
    if(value==false)
    {
        return <button className='btn btn-danger btn-sm'>Trạng thái: tạm dừng</button>
    }
}
const Info = (value:any) => {
    Modal.info({
      title: 'Điều kiện được duyệt - '+value.app,
      content: (
        <div style={{maxHeight:400, overflow:"auto"}}>
            <div style={{color:"#1890ff",padding:5,fontWeight:"bold"}}>Lưu ý: nhập mã giới thiệu <span style={{color:"red", fontWeight:"bold"}}>0969860930</span> để <span style={{color:"red", fontWeight:"bold"}}>nhận thêm quà tặng</span> và được duyệt nhanh hơn</div>
          <img alt={value?.app} style={{ objectFit: "cover", cursor: "pointer", borderRadius: 20,maxWidth:200 }} width='100%' src={value?.logo} key={value?.app}/>
          <div>{value.help.split("\n").map((val:any,index:any)=>(
            <div key={index}>
                {val}
                <br/>
            </div>
          ))}</div>
        </div>
      ),
      onOk() {},
    });
  };

export const UiJob = (props: any) => {
    const [cookies, setCookie,removeCookie] = useCookies(['fullname',"phone","cash","token"]);
    const browser = detect();
    const { data } = props
    const router = useRouter()

    const GetCash=async ()=>{
        if(Object.keys(cookies).length > 0)
        {
            let ret = await APIGetCash({phone:cookies.phone})
            setCookie('cash', ret.data, { path: '/' });
        }
      }
    useEffect(()=>{
        if(!router.isReady) return;
        GetCash()
      },[router.isReady,props])

    const OpenLink = async(link:any,app:any,cash:any)=>{
        if (Object.keys(cookies).length === 0) 
        {
            router.push("/login")

        }
        else
        {
            let info = "name:"+browser?.name +" - os:" +  browser?.os +" - type:" +  browser?.type +" - version:" +  browser?.version 
            await APIPostShareLink({phone:cookies.phone,info:info,link_share:link,name_app:app,cash:cash,token:cookies.token})
        }

    }
    return (
        <div className={styles.UiListNews} >
            <div className={styles.UiTitle}>Các công việc cần làm</div>
            <div style={{color:"#1890ff",padding:5,fontSize:16,fontWeight:"bold"}}>Lưu ý: nhập mã giới thiệu <span style={{color:"red", fontWeight:"bold"}}>0969860930</span> để <span style={{color:"red", fontWeight:"bold"}}>nhận thêm quà tặng</span> và được duyệt nhanh hơn</div>
            <div>
                {
                    data?.map((value: any, index: any) => {
                        if (index%2 ==0)
                        {
                            return (
                                <div style={{ color: "#FFFFFF", cursor: "pointer"}} key={index}>
                                    <div className='row'>
                                        <div className='col-md-6 mt-2'>
                                            <div style={{ color: "orangered", fontWeight: 'bold', paddingBottom: 10, fontSize: 16 }}>{value?.app}</div>
                                            <div className='row'>
                                                <img alt={value?.app} className={'col-6'} style={{ objectFit: "cover", cursor: "pointer", borderRadius: 20,maxWidth:200 }} width='100%' src={value?.logo} />
                                                <div className='col' style={{ justifyContent: "space-between", display: "inline-grid" }}>
                                                    <div>{Status(value?.acctive)}</div>
                                                    <div><div className='btn btn-success btn-sm'>Mã : 0969860930</div></div>
                                                    <div><div className='btn btn-primary btn-sm'> Hoa hồng: {parseInt(value?.rate).toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</div></div>
                                                    <div><button className='btn btn-primary btn-sm' onClick={()=>Info(value)}>Đọc điều kiện duyệt</button></div>
                                                    <div><div className='btn btn-primary btn-sm' onClick={()=>OpenLink(value?.link_share,value?.app,value?.rate)}>Link tải App</div></div>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            data[index+1]?.app && <div className='col-md-6 mt-2'>
                                            <div style={{ color: "orangered", fontWeight: 'bold', paddingBottom: 10, fontSize: 16 }}>{data[index+1]?.app}</div>
                                            <div className='row'>
                                            <img alt={data[index+1]?.app} className={'col-6'} style={{ objectFit: "cover", cursor: "pointer", borderRadius: 20,maxWidth:200 }} width='100%' src={data[index+1]?.logo} />
                                                <div className='col' style={{ justifyContent: "space-between", display: "inline-grid" }}>
                                                    <div>{Status(data[index+1]?.acctive)}</div>
                                                    <div><div className='btn btn-success btn-sm'>Mã : 0969860930</div></div>
                                                    <div><div className='btn btn-primary btn-sm'> Hoa hồng: {parseInt(data[index+1]?.rate).toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</div></div>
                                                    <div><button className='btn btn-primary btn-sm' onClick={()=>Info(data[index+1])}>Đọc điều kiện duyệt</button></div>
                                                    <div><div className='btn btn-primary btn-sm' onClick={()=>OpenLink(data[index+1]?.link_share,data[index+1]?.app,data[index+1]?.rate)}>Link tải App</div></div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                        
                                    </div>
                                </div>
                            )
                        } 
                    }
                    )
                }
            </div>
        </div>
    )
}