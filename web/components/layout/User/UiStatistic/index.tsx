import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import type { ColumnsType, TableProps } from 'antd/es/table';
import { UiListHistory } from './UiListHistory';
import { Tabs } from 'antd';

export const UiStatistic = (props: any) => {
    const { data_success, data_wait, data_exit } = props
    const router = useRouter()
    useEffect(() => {
        if (!router.isReady) return;
    }, [props, router.isReady])

    return (
        <div className={styles.UiListNews}>
            <div className={styles.UiTitle}>Theo dõi lịch sử công việc</div>
            <div className="card-container m-1 mt-3">
                <Tabs type="card" >
                    <Tabs.TabPane tab="Đang kiểm duyệt" key="wait">
                        <UiListHistory data_share={data_wait} />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Đã duyệt" key="success">
                        <UiListHistory data_share={data_success} />

                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Đã hủy" key="exit">
                        <UiListHistory data_share={data_exit} />

                    </Tabs.TabPane>
                </Tabs>
            </div>

        </div>
    )
}
