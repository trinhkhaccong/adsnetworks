import { useEffect } from 'react'
import styles from '../../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import { Table } from 'antd';
import moment  from 'moment';
import type { ColumnsType, TableProps } from 'antd/es/table';

const Status=(value:any)=>{
    if(value==='wait')
    {
        return <button type="button" className="btn btn-warning btn-sm">Đang kiểm duyệt</button>
    }
    if(value==='success')
    {
        return <button type="button" className="btn btn-primary btn-sm">Đã duyệt</button>
    }
    if(value==='exit')
    {
        return <button type="button" className="btn btn-danger btn-sm">Đã hủy</button>
    }
}

const Cash=(status:any,cash:any)=>{
    if(status==='wait')
    {
        return <button type="button" className="btn btn-warning btn-sm">0 VND</button>
    }
    if(status==='success')
    {
        return <button type="button" className="btn btn-primary btn-sm">{cash?.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</button>
    }
    if(status==='exit')
    {
        return <button type="button" className="btn btn-danger btn-sm">0 VND</button>
    }
}
export const UiListHistory = (props: any) => {
    const { data_share } = props
    const router = useRouter()
    useEffect(() => {
        if (!router.isReady) return;
    }, [data_share, router.isReady])
  
    return (
        <div>
           {data_share?.map((value:any,index:any)=>
           {
            if(index%3==0)
            {
                return(
                    <div className='row'>
                        <div className='col-xl-4 mt-2'>
                            <div style={{background:"rgba(255, 255, 255, 0.1)",padding:5,borderRadius:10}}>
                                <div style={{color:"orangered"}}>
                                    {value.name_app}
                                </div>
                                <div className='d-flex justify-content-between mt-1'>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Thời gian: {moment(value.date_time?.replace("T"," ")).format("DD/MM/YYYY ") +value.date_time?.toString()?.slice(11,19)}</div>
                                    <button type="button" className="btn btn-primary btn-sm" onClick={()=>window.open(value?.link_share)}>Link cài App</button>
                                </div>
                                <div className='d-flex justify-content-between mt-1'>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Trạng Thái: {Status(value.status)}</div>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Số tiền nhận: {Cash(value.status,value.cash)}</div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 mt-2'>
                            {data_share[index+1] && 
                            <div style={{background:"rgba(255, 255, 255, 0.1)",padding:5,borderRadius:10}}>
                                <div style={{color:"orangered"}}>
                                    {data_share[index+1]?.name_app}
                                </div>
                                <div className='d-flex justify-content-between mt-1'>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Thời gian: {moment(data_share[index+1].date_time?.replace("T"," ")).format("DD/MM/YYYY ") +data_share[index+1].date_time?.toString()?.slice(11,19)}</div>
                                    <button type="button" className="btn btn-primary btn-sm" onClick={()=>window.open(data_share[index+1]?.link_share)}>Link cài App</button>
                                </div>
                                <div className='d-flex justify-content-between mt-1'>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Trạng Thái: {Status(data_share[index+1].status)}</div>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Số tiền nhận: {Cash(data_share[index+1].status,data_share[index+1].cash)}</div>
                                </div>
                            </div>}
                            
                        </div>
                        <div className='col-xl-4 mt-2'>
                        {data_share[index+2] && 
                            <div style={{background:"rgba(255, 255, 255, 0.1)",padding:5,borderRadius:10}}>
                                <div style={{color:"orangered"}}>
                                    {data_share[index+2]?.name_app}
                                </div>
                                <div className='d-flex justify-content-between mt-1'>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Thời gian: {moment(data_share[index+2].date_time?.replace("T"," ")).format("DD/MM/YYYY ") +data_share[index+2].date_time?.toString()?.slice(11,19)}</div>
                                    <button type="button" className="btn btn-primary btn-sm" onClick={()=>window.open(data_share[index+2]?.link_share)}>Link cài App</button>
                                </div>
                                <div className='d-flex justify-content-between mt-1'>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Trạng Thái: {Status(data_share[index+2].status)}</div>
                                    <div style={{color:"rgba(255, 255, 255, 0.8)"}}>Số tiền nhận: {Cash(data_share[index+2].status,data_share[index+2].cash)}</div>
                                </div>
                            </div>}
                        </div>
                        
                    </div>
                )
            }
           })}
        </div>
    )
}
