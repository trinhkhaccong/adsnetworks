import { useEffect, useState } from 'react'
import styles from '../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import moment from 'moment';
import { APIGetShareUser } from '../../API';
const Status=(value:any)=>{
    if(value==='wait')
    {
        return <button type="button" className="btn btn-warning btn-sm">Đang kiểm duyệt</button>
    }
    if(value==='success')
    {
        return <button type="button" className="btn btn-primary btn-sm">Đã duyệt</button>
    }
    if(value==='exit')
    {
        return <button type="button" className="btn btn-danger btn-sm">Đã hủy</button>
    }
}
export const UiShareUser = (props: any) => {
    const [data,setData] = useState([])
    const getData=async()=>{
        let ret = await APIGetShareUser()
        await setData(ret.data)
    }
    useEffect(() => {
        getData()
    }, [props])
    
    const onSuccess=(value:any)=>{
        console.log(value)
    }

    const onExit =(value:any)=>{
        console.log(value)
    }
    return (
        <div className={styles.UiListNews}>
            <div className='d-flex'>
                <div className={styles.UiTitle}>Danh sách cần duyệt</div>
            </div>
            <div className="card-container m-1 mt-3">
               {data.map((value:any)=><div className='d-flex justify-content-between mt-2' style={{color:"#FFFFFF",background:"rgba(255, 255, 255, 0.1)",padding:5,borderRadius:10,}}>
                <div  className='col'>
                        {
                            value.name_app
                        }
                </div>
                <div  className='col-2'>
                        {
                            value.user_phone
                        }
                </div>
                <div  className='col-2'>
                        {
                            value.ip
                        }
                </div>
                <div  className='col-2'>
                        {
                           Status(value.status)
                        }
                </div>
                <div className='col-1'>
                        {
                            value.cash
                        }
                </div>
                <div className='col-1'>
                        {
                           moment(value.date_time?.replace("T"," ")).format("DD/MM/YYYY ") +value.date_time?.slice(11,19)

                        }
                </div>
                <div className='col-1 d-flex justify-content-between'>
                        <button className='btn btn-primary btn-sm' onClick={()=>onSuccess(value)}>Duyệt</button>
                        <button className='btn btn-danger btn-sm' onClick={()=>onExit(value)}>Hủy</button>
                </div>
                </div>)}
            </div>

        </div>
    )
}
