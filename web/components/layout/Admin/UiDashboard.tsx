import { useEffect } from 'react'
import styles from '../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import { Tabs } from 'antd';
import { UiLinkAccess } from './UiLinkAccess';
import { UiCreateData } from './UiCreateData';
import { UiShareUser } from './UiShareUser';
export const UiDashboard = (props: any) => {
    const { data, data_ios, data_android, data_cash } = props
    const router = useRouter()
    useEffect(() => {

    }, [props])
    
    return (
        <div className={styles.UiListNews}>
            <div className='d-flex'>
                <div className={styles.UiTitle}>Trang quản trị</div>
            </div>
            <div className="card-container m-1 mt-3">
                <Tabs type="card" >
                    <Tabs.TabPane tab="Ios" key="Ios">
                        <UiLinkAccess data={data_ios} />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Android" key="Android">
                        <UiLinkAccess data={data_android} />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Cash" key="Cash">
                        <UiLinkAccess data={data_cash} />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Dữ liệu" key="data">
                        <UiCreateData />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Người dùng" key="exit">
                        <div style={{ background: "rgba(255,255,255,0.1)", textAlign: "center", color: "#FFFFFF", borderRadius: 10 }} className="col-md-2 mt-2">
                            <div className={styles.UiTitle}>Tổng số người dùng</div>
                            <div>
                                {
                                    data
                                } người
                            </div>
                        </div>
                        <UiShareUser/>
                    </Tabs.TabPane>
                </Tabs>
            </div>

        </div>
    )
}
