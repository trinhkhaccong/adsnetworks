import { useEffect, useState } from 'react'
import styles from '../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import { Table } from 'antd';
import moment from 'moment';
import type { ColumnsType, TableProps } from 'antd/es/table';
import { APIPostInsetAccess } from '../../API';
import { Select } from 'antd';
import { useCookies } from 'react-cookie';
import { Switch } from 'antd';
import { Md5 } from 'ts-md5';
const { Option } = Select;

export const UiCreateData = (props: any) => {
    const [cookie, setCookie] = useCookies(['phone'])
    const [type, setType] = useState("")
    const [logo,setLogo] = useState("")
    const [rate, setRate] = useState("")
    const [app,setApp] = useState("")
    const [help,setHelp] = useState("")
    const [link_share, setLinkShare] = useState("")
    const [acctive,setAcctive] = useState(true)

    const [msg, setMsg] = useState("")
    const router = useRouter()
    useEffect(() => {
        if (!router.isReady) return;
    }, [router.isReady])

    const handleChange = async (value: string) => {
        await setType(value)
    };
    const onSave = async () => {
        await setMsg("")
        let param:any = {
            id:Md5.hashStr(link_share),
            logo:logo,
            rate:rate,
            app:app,
            type:type,
            help:help,
            link_share:link_share,
            acctive:acctive
        }
        let ret = await APIPostInsetAccess(param)
        await setMsg(ret.messenger)
        if (ret.status == 1) {
            setTimeout(function () {
                window.open("/admin", "_parent")
            }, 1000);
        }
    }
    const onChange = async(checked: boolean) => {
        await setAcctive(checked)
      };
    return (

        <div className='pb-2 mt-2'>
            <div className={styles.UiTitle}>Tạo thông tin chiến dịch</div>
            <div style={{color:"#0070f3",padding:5}}>{msg}</div>
            <Select showSearch style={{ width: '100%' }} placeholder="Chọn chiến dịch" onChange={handleChange}>
                <Option key="ios">Cài đặt app cho IOS</Option>
                <Option key="android">Cài đặt app cho ANDROID</Option>
                <Option key="ios,android">Cài đặt app cho cả IOS và ANDROID</Option>
                <Option key="cash">Dịch vụ tài chính</Option>
            </Select>
            <div  className='mt-2'>
                <div style={{ color: "#FFF" }}>
                    Tên App
                </div>
                <input className='col-12' placeholder='VD: Emeeting - android' onChange={(e) => setApp(e.target.value)} />
            </div>
            <div  className='mt-2'>
                <div style={{ color: "#FFF" }}>
                   Logo
                </div>
                <input className='col-12' placeholder='VD: https://meeting.png' onChange={(e) => setLogo(e.target.value)} />
            </div>
            <div  className='mt-2'>
                <div style={{ color: "#FFF" }}>
                    Link share
                </div>
                <input className='col-12' placeholder='VD: https://shorten.asia/vwv2K1Ha' onChange={(e) => setLinkShare(e.target.value)} />
            </div>
            <div  className='mt-2'>
                <div style={{ color: "#FFF" }}>
                    Số tiền
                </div>
                <input type="number" className='col-12' placeholder='VD: 1000000' onChange={(e) => setRate(e.target.value)} />
            </div>
            <div className='mt-2'>
                <div style={{ color: "#FFF" }}>
                    Ghi chú
                </div>
                <textarea rows={5} className='col-12' placeholder='VD: Hồ Chí Minh' onChange={(e) => setHelp(e.target.value)} />
            </div>
            <div className='d-flex mt-2'>
                <div style={{ color: "#FFF" ,paddingRight:15}}>
                    Trạng thái
                </div>
                <Switch defaultChecked onChange={onChange} />
            </div>
            <div className='d-flex mt-2'>
                <div className='col'></div>
                <button type="button" className="btn btn-primary" onClick={onSave}>Lưu tài khoản</button>
            </div>
        </div>
    )
}
