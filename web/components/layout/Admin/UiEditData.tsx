import { useEffect, useState } from 'react'
import styles from '../../../styles/Home.module.css'
import { useRouter } from 'next/router';
import { APIPostInsetAccess } from '../../API';
import { Select } from 'antd';
import { useCookies } from 'react-cookie';
import { Switch } from 'antd';
import { Md5 } from 'ts-md5';
const { Option } = Select;

export const UiEditData = (props: any) => {
    const {data} = props
    const [cookie, setCookie] = useCookies(['phone'])
    const [type, setType] = useState(data.type)
    const [logo,setLogo] = useState(data.logo)
    const [rate, setRate] = useState(data.rate)
    const [app,setApp] = useState(data.app)
    const [help,setHelp] = useState(data.help)
    const [link_share, setLinkShare] = useState(data.link_share)
    const [acctive,setAcctive] = useState(data.acctive)
    const [msg, setMsg] = useState("")

    const handleChange = async (value: string) => {
        await setType(value)
    };
    const onSave = async () => {
        await setMsg("")
        let param:any = {
            id:data.id,
            logo:logo,
            rate:rate,
            app:app,
            type:type,
            help:help,
            link_share:link_share,
            acctive:acctive
        }
        let ret = await APIPostInsetAccess(param)
        await setMsg(ret.messenger)
        if (ret.status == 1) {
            setTimeout(function () {
                window.open("/admin", "_parent")
            }, 1000);
        }
    }
    const onChange = async(checked: boolean) => {
        await setAcctive(checked)
      };
    return (

        <div className='pb-2 mt-2'>
            <div className={styles.UiTitle}>Tạo thông tin chiến dịch</div>
            <div style={{color:"#0070f3",padding:5}}>{msg}</div>
            <Select defaultValue={type} showSearch style={{ width: '100%' }} placeholder="Chọn chiến dịch" onChange={handleChange}>
                <Option key="ios">Cài đặt app cho IOS</Option>
                <Option key="android">Cài đặt app cho ANDROID</Option>
                <Option key="ios,android">Cài đặt app cho cả IOS và ANDROID</Option>
                <Option key="cash">Dịch vụ tài chính</Option>
            </Select>
            <div  className='mt-2'>
                <div>
                    Tên App
                </div>
                <input className='col-12' placeholder='VD: Emeeting - android' value={app} onChange={(e) => setApp(e.target.value)} />
            </div>
            <div  className='mt-2'>
                <div>
                   Logo
                </div>
                <input className='col-12' placeholder='VD: https://meeting.png' value={logo} onChange={(e) => setLogo(e.target.value)} />
            </div>
            <div  className='mt-2'>
                <div>
                    Link share
                </div>
                <input className='col-12' placeholder='VD: https://shorten.asia/vwv2K1Ha' value={link_share} onChange={(e) => setLinkShare(e.target.value)} />
            </div>
            <div  className='mt-2'>
                <div>
                    Số tiền
                </div>
                <input type="number" className='col-12' placeholder='VD: 1000000' value={rate} onChange={(e) => setRate(e.target.value)} />
            </div>
            <div className='mt-2'>
                <div>
                    Ghi chú
                </div>
                <textarea rows={5} className='col-12' placeholder='VD: Hồ Chí Minh' value={help} onChange={(e) => setHelp(e.target.value)} />
            </div>
            <div className='d-flex mt-2'>
                <div style={{ color: "#FFF" ,paddingRight:15}}>
                    Trạng thái
                </div>
                <Switch defaultChecked={acctive} onChange={onChange} />
            </div>
            <div className='d-flex mt-2'>
                <div className='col'></div>
                <button type="button" className="btn btn-primary" onClick={onSave}>Lưu tài khoản</button>
            </div>
        </div>
    )
}
