import axios from "axios"

export const APIGetListNews=(from:any)=>{
    return MethodGet("/api/get-list-news?from="+from.toString())
}
export const APIGetNews=(link:any)=>{
    return MethodGet("/api/get-news?link="+link)
}
export const APIGetUpdateView=(param:any)=>{
    const {link} = param
    return MethodGet("/api/update-views?link="+link)
}
export const APIGetNewsTrend=()=>{
    return MethodGet("/api/get-news-trend")
}
export const APIGetAccess=(props:any)=>{
    const {type} = props
    return MethodGet("/api/get_access_link?type="+type)
}
export const APIGetInterest=(props:any)=>{
    const {title} = props
    return MethodGet("/api/get-interest-news?title="+title)
}
//user
export const APIGetShareLink=(props:any)=>{
    const {phone,token} = props
    return MethodGet("/api/user/get-share-link?phone="+phone,token)
}
export const APIGetInfoBank=(props:any)=>{
    const {phone} = props
    return MethodGet("/api/user/get_info_bank?phone="+phone)
}

export const APIGetHistoryBank=(props:any)=>{
    const {phone} = props
    return MethodGet("/api/user/get_info_history_bank?phone="+phone)
}
export const APIGetCash=(props:any)=>{
    const {phone} = props
    return MethodGet("/api/user/get_cash?phone="+phone)
}
export const APIGetNumberUser=()=>{
    return MethodGet("/api/admin/get_number_user")
}
export const APIGetAccessTrade=()=>{
    return MethodGet("/api/admin/get-access-trade")
}
export const APIGetShareUser=()=>{
    return MethodGet("/api/admin/get_share_user")
}

const MethodGet=async(url:string,token:any="")=>{
    let ret = await axios({
        headers: {
            'Content-Type':'application/json',
            'Acess-Control-Allow-Origin':'*',
            'Authorization':`${token}`,
            'Accept': "application/json"
            },
        method:"GET",
        url:"http://localhost:3000"+url
    })
    return ret.data
}