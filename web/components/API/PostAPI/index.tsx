import axios from "axios"

export const APIPostInterest=(props:any)=>{
    return MethodPost("/api/get-interest-news",props)
}
export const APIPostRegister=(props:any)=>{
    return MethodPost("/api/user/post-register",props)
}
export const APIPostActive=(props:any)=>{
    return MethodPost("/api/user/post-active",props)
}
export const APIPosLogin=(props:any)=>{
    return MethodPost("/api/user/post-login",props)
}
export const APIPostShareLink=(props:any)=>{
    return MethodPost("/api/user/post_share_link",props)
}
export const APIPostInsertBank=(props:any)=>{
    return MethodPost("/api/user/post_insert_bank",props)
}
export const APIPostWithdraw=(props:any)=>{
    return MethodPost("/api/user/post_withdraw",props)
}

//admin
export const APIPostInsetAccess=(props:any)=>{
    return MethodPost("/api/admin/post_insert_access",props)
}

const MethodPost=async(url:string,data:any)=>{
    let ret = await axios({
        method:"POST",
        headers: {
            'Content-Type':'application/json',
            'Acess-Control-Allow-Origin':'*',
            'Authorization':`${data.token}`,
            'Accept': "application/json"
            },
        url:"http://localhost:3000"+url,
        data:data
    })
    return ret.data
}