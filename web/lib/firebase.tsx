import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyBd3tuzHkm5ARrvh5KLjY_j9p8dAPF366k",
  authDomain: "ads-social-networks.firebaseapp.com",
  projectId: "ads-social-networks",
  storageBucket: "ads-social-networks.appspot.com",
  messagingSenderId: "308008569741",
  appId: "1:308008569741:web:abc0f132212966309ccbc4",
  measurementId: "G-L230V8LJJZ"
};
if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}
const auth = firebase.auth();
export { auth, firebase };