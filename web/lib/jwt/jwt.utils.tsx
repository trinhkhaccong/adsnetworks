import { sign, SignOptions,verify } from 'jsonwebtoken';
import * as fs from 'fs';
const privateKey = "congtk*sdfjbsmd&fbqwdnjsmfjgwqkrdhnekdj@sfbcmsd4161$345bncvzxczxcskdjf";

/**
 * generates JWT used for local testing
 */
export function generateToken(payload:any) {
  // information to be encoded in the JWT
  
  // read private key value

  const signInOptions: SignOptions = {
    // RS256 uses a public/private key pair. The API provides the private key
    // to generate the JWT. The client gets a public key to validate the
    // signature
    algorithm: "HS512",
    expiresIn: "7d",
  };

  // generate JWT
  return sign(payload, privateKey, signInOptions)
};

export const verifyToken=(token:any)=>{
    try{
        verify(token,privateKey);
        return true;
    }
    catch(e)
    {
        return false
    }
}