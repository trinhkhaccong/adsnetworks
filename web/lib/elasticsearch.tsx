import {Client } from 'elasticsearch'
const client = new Client({
    host: "185.219.132.66:9200",
    log: "error"
  });

export const SearchNews=async(query:any)=>{
    const result = await client.search({
        index: 'data_news',
        body: query
      })
      let data:any=[]
      result['hits']['hits'].map(value=>{
        data.push(value['_source'])
      })
      return data;
}

export const GetNews=async(query:any)=>{
  const result = await client.search({
      index: 'data_news',
      body: query
    })
    let data:any=[]
    result['hits']['hits'].map(value=>{
      data.push(value['_source'])
    })
    return data[0];
}

export const InSert=async(query:any,_id:any)=>{
  const result = await client.index({
      index: 'accesstrade',
      type: "_doc",
      id: _id,
      body: query
    })
    return result;
}
export const SearchAccess=async(query:any)=>{
  const result = await client.search({
      index: 'accesstrade',
      body: query
    })
    let data:any=[]
    result['hits']['hits'].map(value=>{
      data.push(value['_source'])
    })
    return data;
}
