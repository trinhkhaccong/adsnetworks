-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th9 21, 2022 lúc 08:55 AM
-- Phiên bản máy phục vụ: 10.3.36-MariaDB-cll-lve
-- Phiên bản PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `doithec4_news`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_bank_user`
--

CREATE TABLE `tb_bank_user` (
  `id` int(11) NOT NULL,
  `phone` text NOT NULL,
  `bank_name` text NOT NULL,
  `bank_user_name` text NOT NULL,
  `bank_address` text NOT NULL,
  `bank_number` text NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tb_bank_user`
--

INSERT INTO `tb_bank_user` (`id`, `phone`, `bank_name`, `bank_user_name`, `bank_address`, `bank_number`, `date_time`) VALUES
(1, '0969860930', 'BIDV', 'Trinh Khac Cong', 'Ha Noi', '09698630930', '2022-09-20 13:28:38'),
(2, '0969860930', 'TECHCOMBANK', 'TRINH KHAC CONG', 'Back Khoa', '02365134861', '2022-09-20 13:38:08');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_ip`
--

CREATE TABLE `tb_ip` (
  `id` int(11) NOT NULL,
  `phone` text NOT NULL,
  `ip` text NOT NULL,
  `info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_rate`
--

CREATE TABLE `tb_rate` (
  `id` int(11) NOT NULL,
  `name_app` text NOT NULL,
  `link_share` text NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_rate`
--

INSERT INTO `tb_rate` (`id`, `name_app`, `link_share`, `rate`) VALUES
(1, 'TNEX - Ngân hàng số thế hệ mới - App Ios', 'https://shorten.asia/5u9u16ZC', 20000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_share`
--

CREATE TABLE `tb_share` (
  `id` int(11) NOT NULL,
  `name_app` text NOT NULL,
  `link_share` text NOT NULL,
  `user_phone` text NOT NULL,
  `ip` text NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp(),
  `detail` text NOT NULL,
  `status` text NOT NULL,
  `cash` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_share`
--

INSERT INTO `tb_share` (`id`, `name_app`, `link_share`, `user_phone`, `ip`, `date_time`, `detail`, `status`, `cash`) VALUES
(1, 'TNEX - Ngân hàng số thế hệ mới - App Ios', 'https://shorten.asia/5u9u16ZC', '0969860930', '10.2.44.110', '2022-09-19 13:39:42', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'success', 25000),
(2, 'MB Bank - App Ios', 'https://shorten.asia/Nd3cMf8B', '0969860930', '127.0.0.1', '2022-09-19 14:07:41', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'wait', 15000),
(3, 'CAKE - Ngân hàng số - App Ios', 'https://shorten.asia/eAqHvu9s', '0969860930', '127.0.0.1', '2022-09-19 14:09:55', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'wait', 15000),
(4, 'TNEX - Ngân hàng số thế hệ mới - App Ios', 'https://shorten.asia/5u9u16ZC', '0969860930', '127.0.0.1', '2022-09-19 14:10:14', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'success', 15000),
(5, 'VPBank NEO', 'https://shorten.asia/vHNB3MsU', '0969860930', '127.0.0.1', '2022-09-19 14:10:17', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'wait', 40000),
(6, 'MB Bank - App Android', 'https://shorten.asia/M1CJgaaf', '0969860930', '127.0.0.1', '2022-09-19 14:11:26', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'wait', 20000),
(7, 'TNEX - Ngân hàng số thế hệ mới - App Android', 'https://shorten.asia/19WM7zFx', '0969860930', '127.0.0.1', '2022-09-19 14:11:27', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'wait', 15000),
(8, 'VPBank NEO', 'https://shorten.asia/vHNB3MsU', '0969860930', '127.0.0.1', '2022-09-19 14:11:29', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'exit', 25000),
(9, 'CAKE - Ngân hàng số - App Android', 'https://shorten.asia/5tKnSMb1', '0969860930', '127.0.0.1', '2022-09-19 14:11:30', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'exit', 30000),
(10, 'CAKE - Ngân hàng số - App Android', 'https://shorten.asia/5tKnSMb1', '0969860930', '127.0.0.1', '2022-09-19 14:12:34', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'wait', 30000),
(11, 'MB Bank - App Android', 'https://shorten.asia/M1CJgaaf', '0969860930', '127.0.0.1', '2022-09-19 14:12:36', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'wait', 15000),
(12, 'MB Bank - App Ios', 'https://shorten.asia/Nd3cMf8B', '0969860930', '127.0.0.1', '2022-09-19 14:12:39', 'name:chrome - os:Windows 10 - type:browser - version:105.0.0', 'wait', 25000),
(13, 'VPBank NEO', 'https://shorten.asia/vHNB3MsU', '0969860930', '127.0.0.1', '2022-09-20 01:03:35', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 20000),
(14, 'MB Bank - App Ios', 'https://shorten.asia/Nd3cMf8B', '0969860930', '127.0.0.1', '2022-09-21 00:24:21', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 15000),
(15, 'MB Bank - App Ios', 'https://shorten.asia/Nd3cMf8B', '0969860930', '127.0.0.1', '2022-09-21 00:36:03', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 15000),
(16, 'MB Bank - App Ios', 'https://shorten.asia/Nd3cMf8B', '0969860930', '127.0.0.1', '2022-09-21 00:36:28', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 15000),
(17, 'TNEX - Ngân hàng số thế hệ mới - App Ios', 'https://shorten.asia/5u9u16ZC', '0969860930', '127.0.0.1', '2022-09-21 00:41:45', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 15000),
(18, 'MB Bank - App Ios', 'https://shorten.asia/Nd3cMf8B', '0969860930', '127.0.0.1', '2022-09-21 00:54:16', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 15000),
(19, 'Techcombank Mobile  - IOS', 'https://shorten.asia/vRnSSkNn', '0969860930', '127.0.0.1', '2022-09-21 00:54:31', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 40000),
(20, 'MB Bank - App Ios', 'https://shorten.asia/Nd3cMf8B', '0969860930', '127.0.0.1', '2022-09-21 00:55:08', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 15000),
(21, 'MB Bank - App Ios', 'https://shorten.asia/Nd3cMf8B', '0969860930', '127.0.0.1', '2022-09-21 00:56:17', 'name:chrome - os:Windows 7 - type:browser - version:105.0.0', 'wait', 15000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `fullname` text CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `phone` text NOT NULL,
  `pass` text NOT NULL,
  `code` text NOT NULL,
  `token` text NOT NULL,
  `code_share` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `cash` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tb_user`
--

INSERT INTO `tb_user` (`id`, `fullname`, `phone`, `pass`, `code`, `token`, `code_share`, `is_active`, `is_admin`, `cash`) VALUES
(6, 'Trịnh Khắc Công', '0969860930', 'a6aed5d9d60f2f1e22279c40134768ff', '249399', '', 'o2LmSFTfnU', 1, 1, 10000000),
(7, 'Gửi thẻ cào', '0969860325', 'a6aed5d9d60f2f1e22279c40134768ff', '550040', '', 'xz26nT1GTT', 1, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_view`
--

CREATE TABLE `tb_view` (
  `id` int(11) NOT NULL,
  `root_link` text NOT NULL,
  `date` date NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_view`
--

INSERT INTO `tb_view` (`id`, `root_link`, `date`, `views`) VALUES
(90, 'thanh-lap-to-cong-tac-dieu-tra-vu-sap-tuong-tai-khu-cong-nghiep-nhon-hoa-post715736', '2022-09-20', 2),
(91, 'nang-cao-nang-luc-dao-tao-dap-ung-nhu-cau-phat-trien-post715720', '2022-09-20', 8),
(92, 'nam-giai-phap-giup-thuc-day-thuong-mai-giua-viet-nam-va-italy-post715740', '2022-09-20', 2),
(95, 'nga-de-nghi-cung-cap-lua-my-cho-pakistan-de-doi-pho-voi-lu-lut-post715739', '2022-09-20', 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_withdraw`
--

CREATE TABLE `tb_withdraw` (
  `id` int(11) NOT NULL,
  `phone` text NOT NULL,
  `bank_name` text NOT NULL,
  `bank_number` text NOT NULL,
  `cash` text NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp(),
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tb_withdraw`
--

INSERT INTO `tb_withdraw` (`id`, `phone`, `bank_name`, `bank_number`, `cash`, `date_time`, `status`) VALUES
(1, '0969860930', 'BIDV', '09698630930', '10000', '2022-09-20 15:16:52', 'wait'),
(2, '0969860930', 'BIDV', '09698630930', '10000', '2022-09-20 15:18:28', 'success'),
(3, '0969860930', 'BIDV', '09698630930', '10000', '2022-09-20 15:19:07', 'wait'),
(4, '0969860930', 'BIDV', '09698630930', '10000', '2022-09-20 15:19:08', 'wait'),
(6, '0969860930', 'BIDV', '09698630930', '10000', '2022-09-20 15:19:09', 'exit'),
(7, '0969860930', 'BIDV', '09698630930', '10000', '2022-09-20 15:19:10', 'wait'),
(8, '0969860930', 'TECHCOMBANK', '02365134861', '10000', '2022-09-20 15:43:53', 'wait'),
(9, '0969860930', 'TECHCOMBANK', '02365134861', '10000', '2022-09-20 15:47:44', 'wait'),
(10, '0969860930', 'TECHCOMBANK', '02365134861', '10000', '2022-09-20 15:48:28', 'wait');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tb_bank_user`
--
ALTER TABLE `tb_bank_user`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_ip`
--
ALTER TABLE `tb_ip`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_rate`
--
ALTER TABLE `tb_rate`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_share`
--
ALTER TABLE `tb_share`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_view`
--
ALTER TABLE `tb_view`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_withdraw`
--
ALTER TABLE `tb_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tb_bank_user`
--
ALTER TABLE `tb_bank_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tb_ip`
--
ALTER TABLE `tb_ip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_rate`
--
ALTER TABLE `tb_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tb_share`
--
ALTER TABLE `tb_share`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `tb_view`
--
ALTER TABLE `tb_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT cho bảng `tb_withdraw`
--
ALTER TABLE `tb_withdraw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
