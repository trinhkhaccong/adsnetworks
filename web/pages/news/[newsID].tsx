import type { NextPage } from 'next'
import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import {UiHead, UiInterest, UiInfoNews,UiTrend,UiMenu} from '../../components/layout'
import { APIGetNews,APIGetUpdateView,APIGetNewsTrend,APIPostInterest } from '../../components/API'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router';
import { Space, Spin } from 'antd';
interface NewsProp
{
  news:any;
  trend:any;
  list_news:any;
}
const News=(props:NewsProp)=>{
  const {news,trend,list_news} = props
  const router = useRouter()  
  const scrollToTop = () => {
    window.scrollTo(0, 0);
  };
  useEffect(()=>{
    if(!router.isReady) return;
    scrollToTop();
  },[router.isReady,router.query.newsID])
    return(
        <div className={styles.main}>
      <Head>
        <title>ADS Social Networks - Lắng Nghe Mạng Xã Hội</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <UiHead />
        <div className='menu-mobile'><UiMenu /></div>
        <div className={styles.UiBody} style={{ padding: 5 }}>
          <div className='row mt-3'>
            <div className='col-md-8 mb-3'>
            <div >
            <UiInfoNews data = {news}/>
            <UiInterest data = {list_news}/>
          </div>
            </div>
          
            <div className='col-md-4'>
            <div>
            <UiTrend data = {trend}/>
          </div>
            </div>
          </div>
          
        </div>
      </main>
    </div>
    )
}
export const getServerSideProps = async (context: any) => {
  let list = context.params.newsID
  await APIGetUpdateView({link:list})
    let ret = await APIGetNews(context.query.newsID)
    let ret_trend = await APIGetNewsTrend()
    const news =await ret.data;
    let ret_new = await APIPostInterest({title:news?.meta_title})
    const list_news = await ret_new.data
    const trend = await ret_trend.data
  return { props: {news,list_news,trend } }
}

export default News