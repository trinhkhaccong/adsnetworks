import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {SearchNews} from "../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
        let query:any =
        {
            size:20,
            from:req.query.from,
            sort : [
                {
                    "date" : "desc"
                 }
            ],
			query: {
                match_all: {}
				}
			}
        const result = await SearchNews(query);
        return res.status(200).json({ data: result })
    
}
