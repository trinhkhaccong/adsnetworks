import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {GetNews} from "../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
        let query:any =
        {
            sort : [
                {
                    "date" : "desc"
                 }
            ],
			query: {
                query_string: {
                    query: "link:"+req.query.link,
                    type: "phrase"
                  }
				}
			}
        const result = await GetNews(query);
        return res.status(200).json({ data: result })
    
}
