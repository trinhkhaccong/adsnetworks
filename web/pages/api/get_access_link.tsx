import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {SearchAccess} from "../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
        let query:any =
        {
            size:40,
			query: {
                query_string: {
                    query: "type:"+req.query.type,
                    type: "phrase"
                  }
				}
			}
        const result = await SearchAccess(query);
        return res.status(200).json({ data: result })
    
}