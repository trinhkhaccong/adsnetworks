import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, excuteQueryOb, auth} from "../../../lib"
interface CashProp
{
    cash:any;
}
export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )

    let phone = req.query.phone
    const result_cash:CashProp = await excuteQueryOb({
        query: 'SELECT cash FROM tb_user WHERE ?',
        values: [{phone:phone}],
    });
    
    return res.status(200).json({ status:0,messenger:"",data:result_cash.cash })
}
