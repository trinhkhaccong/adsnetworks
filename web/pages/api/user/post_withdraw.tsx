import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, excuteQueryOb, auth} from "../../../lib"
interface CashProp
{
    cash:any;
}
export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )

    let phone = req.body.phone
    let bank_name = req.body.bank_name
    let bank_number = req.body.bank_number
    let cash = req.body.cash
    const result_cash:CashProp = await excuteQueryOb({
        query: 'SELECT cash FROM tb_user WHERE ?',
        values: [{phone:phone}],
    });
    let cash_update = result_cash?.cash - parseInt(cash)
    if(cash_update>0)
    {   
            const result_check:any = await excuteQuery({
                query: 'INSERT INTO tb_withdraw (phone, bank_name, bank_number,cash,status) VALUES (?,?,?,?,?)',
                values: [phone,bank_name,bank_number,cash,"wait"],
            });

            const result:any = await excuteQuery({
                query: 'UPDATE tb_user SET ? WHERE ?',
                values: [{cash:cash_update},{phone:phone}],
            });
            return res.status(200).json({ status:1,messenger:"Gửi thông tin rút tiền thành công",cash:cash_update })
    }
    else
    {
        return res.status(200).json({ status:0,messenger:"Gửi thông tin rút tiền bị lỗi" })
    }
}
