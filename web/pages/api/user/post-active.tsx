import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, firebase, auth} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    
    let phone = req.body.phone
    let code = req.body.code
    const result_check:any = await excuteQuery({
        query: 'SELECT fullname FROM tb_user WHERE ? AND ?',
        values: [{code:code},{phone:phone}],
    });
    if(result_check.length == 1)
    {   
        try{
            const result:any = await excuteQuery({
                query: 'UPDATE tb_user SET is_active=1 WHERE ? AND ?',
                values: [{code:code},{phone:phone}],
            });
            return res.status(200).json({ status:1,messenger:"Mã kích hoạt thành công" })
        }
        catch(err)
        {
            return res.status(200).json({ status:0,messenger:"Mã kích hoạt bị lỗi" })

        }
    }
    else
    {
        return res.status(200).json({ status:0,messenger:"Mã kích hoạt không đúng" })
    }
}
