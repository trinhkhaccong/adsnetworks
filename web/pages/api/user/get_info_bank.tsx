import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, firebase, auth} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    let phone = req.query.phone
    const result:any = await excuteQuery({
        query: 'SELECT * FROM tb_bank_user WHERE ?',
        values: [{phone:phone}],
    }); 
    return res.status(200).json({ status:1,data:result})
}
