import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, firebase, auth} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    
    let phone = req.body.phone
    let bank_name = req.body.bank_name
    let bank_user_name=req.body.bank_user_name
    let bank_number = req.body.bank_number
    let bank_address = req.body.bank_address
    const result_check:any = await excuteQuery({
        query: 'INSERT INTO tb_bank_user (phone, bank_name, bank_user_name,bank_number,bank_address) VALUES (?,?,?,?,?)',
        values: [phone,bank_name,bank_user_name,bank_number,bank_address],
    });
    if(result_check)
    {   
            return res.status(200).json({ status:1,messenger:"Lưu thông tin bank thành công" })
    }
    else
    {
        return res.status(200).json({ status:0,messenger:"Lưu thông tin bank bị lỗi" })
    }
}
