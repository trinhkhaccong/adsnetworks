import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, generateToken , auth} from "../../../lib"
const payload = {
    name: 'Andrés Reales',
    userId: 123,
    accessTypes: [
      'getTeams',
      'addTeams',
      'updateTeams',
      'deleteTeams'
    ]
  };
export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    
    let phone = req.body.phone
    let pass = req.body.pass
    const result_check:any = await excuteQuery({
        query: 'SELECT cash,phone,fullname,is_admin FROM tb_user WHERE ? AND ? AND is_active=1',
        values: [{phone:phone},{pass:Md5.hashStr(pass)}],
    });
    if(result_check.length == 1)
    {   
        let param:any={
            cash: result_check[0]?.cash,
            phone: result_check[0]?.phone,
            fullname: result_check[0]?.fullname,
            is_admin: result_check[0]?.is_admin
        }
            return res.status(200).json({ status:1,data:result_check[0],messenger:"Đăng nhập thành công",token:generateToken(param)})
    }
    else
    {
        return res.status(200).json({ status:0,messenger:"Nhập sai tài khoản hoặc mật khẩu" })
    }
}
