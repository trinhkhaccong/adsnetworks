import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, sendSMS, auth} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    
    let fullname = req.body.fullname
    let phone = req.body.phone
    let pass = req.body.pass
    const result_check:any = await excuteQuery({
        query: 'SELECT fullname FROM tb_user WHERE ?',
        values: [{phone:phone}],
    });
    if(result_check.length == 0)
    {   
        try{
            const code:any = CodeActive(6)
            const result:any = await excuteQuery({
                query: 'INSERT INTO tb_user (fullname, phone, pass,code,code_share,is_active,is_admin) VALUES (?,?,?,?,?,?,?)',
                values: [fullname,phone,Md5.hashStr(pass),code,genRandonString(10),0,0],
            });
            sendSMS([phone],"Ma kich hoat tai khoan Make Money Online la " +code , 2, '');
            return res.status(200).json({ status:1,messenger:"Đăng ký tài khoản thành công" })
        }
        catch(err)
        {
            return res.status(200).json({ status:0,messenger:"Đăng ký tài khoản bị lỗi, vui lòng liên hệ admin" })

        }
        
    }
    else
    {
        return res.status(200).json({ status:0,messenger:"Đã tồn tại số điện thoại" })
    }
}


const genRandonString=(length:any)=> {
    var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charLength = chars.length;
    var result = '';
    for ( var i = 0; i < length; i++ ) {
       result += chars.charAt(Math.floor(Math.random() * charLength));
    }
    return result;
 }

 const CodeActive=(length:any)=> {
    var chars = '0123456789';
    var charLength = chars.length;
    var result = '';
    for ( var i = 0; i < length; i++ ) {
       result += chars.charAt(Math.floor(Math.random() * charLength));
    }
    return result;
 }
