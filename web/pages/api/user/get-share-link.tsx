import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, verifyToken, auth} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    var authheader = req.headers.authorization;
    if(verifyToken(authheader))
    {
        let phone = req.query.phone
        const result_success:any = await excuteQuery({
            query: 'SELECT cash,name_app,link_share,date_time,status FROM tb_share WHERE ? AND ?',
            values: [{user_phone:phone},{status:"success"}],
        }); 
    
        const result_wait:any = await excuteQuery({
            query: 'SELECT cash,name_app,link_share,date_time,status FROM tb_share WHERE ? AND ?',
            values: [{user_phone:phone},{status:"wait"}],
        }); 
    
        const result_exit:any = await excuteQuery({
            query: 'SELECT cash,name_app,link_share,date_time,status FROM tb_share WHERE ? AND ?',
            values: [{user_phone:phone},{status:"exit"}],
        });
        return res.status(200).json({ status:1,data_success:result_success,data_wait:result_wait,data_exit:result_exit})
    }
    else
    {
        return res.status(200).json({ status:2,data_success:[],data_wait:[],data_exit:[]})

    }
    
}
