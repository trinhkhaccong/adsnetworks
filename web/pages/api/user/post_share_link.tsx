import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import requestIp from 'request-ip';

import {Md5} from 'ts-md5';
import {excuteQuery, verifyToken, auth} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    var authheader = req.headers.authorization;
    if(verifyToken(authheader))
    {
        let phone = req.body.phone
        let info = req.body.info
        let link_share=req.body.link_share
        let name_app = req.body.name_app
        let ip:any = requestIp.getClientIp(req)
        let cash = req.body.cash
        ip = ip?.replace("::1","127.0.0.1")
        const result_check:any = await excuteQuery({
            query: 'INSERT INTO tb_share (link_share, user_phone, ip, detail,name_app,cash,status) VALUES (?,?,?,?,?,?,?)',
            values: [link_share,phone,ip,info,name_app,cash,"wait"],
        });
        if(result_check)
        {   
                return res.status(200).json({ status:1,messenger:"" })
        }
        else
        {
            return res.status(200).json({ status:0,messenger:"update bị lỗi" })
        }
    }
    else
        {
            return res.status(200).json({ status:2,messenger:"update bị lỗi" })
        }

}
