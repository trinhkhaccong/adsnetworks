import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {SearchNews,excuteQuery} from "../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    const result_week:any = await excuteQuery({
        query: 'SELECT root_link FROM tb_view WHERE date BETWEEN curdate()-7 AND curdate() GROUP BY tb_view.root_link ORDER BY views DESC LIMIT 15',
        values: [],
      });
      let string_query:any=""
      result_week.map((value:any,index:any)=>{
        if(index==0)
        {
            string_query=string_query+"(link:"+value.root_link+")"

        }
        else
        {
            string_query=string_query+" OR (link:"+value.root_link+")"
        }
      })
        let query:any =
        {
            size:10,
            sort : [
                {
                    "date" : "desc"
                 }
            ],
			query: {
                query_string: {
                    query: string_query,
                    type: "phrase"
                  }
				}
			}
        const result = await SearchNews(query);
        return res.status(200).json({ data: result })
    
}
