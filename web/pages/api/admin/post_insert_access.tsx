import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, InSert, auth} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    const result_check:any = await InSert(req.body,req.body.id);
    if(result_check)
    {   
            return res.status(200).json({ status:1,messenger:"Lưu thông tin thành công",data:result_check })
    }
    else
    {
        return res.status(200).json({ status:0,messenger:"Lưu thông tin bị lỗi" })
    }
}
