import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, excuteQueryOb, auth} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )

    const result = await excuteQuery({
        query: 'SELECT * FROM tb_share ORDER BY date_time desc ',
        values: [],
    });
    
    return res.status(200).json({ status:1,messenger:"",data:result })
}
