import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {Md5} from 'ts-md5';
import {excuteQuery, excuteQueryOb, auth} from "../../../lib"
interface CashProp
{
    count:any;
}
export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )

    const result:CashProp = await excuteQueryOb({
        query: 'SELECT COUNT(phone) as count FROM tb_user WHERE ?',
        values: [{is_admin:0}],
    });
    
    return res.status(200).json({ status:0,messenger:"",data:result.count })
}
