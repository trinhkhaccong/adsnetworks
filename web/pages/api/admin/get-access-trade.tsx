import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {SearchAccess} from "../../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
        let query_ios:any =
        {
            size:40,
			query: {
                query_string: {
                    query: "type:ios",
                    type: "phrase"
                  }
				}
			}
        const result_ios = await SearchAccess(query_ios);

        let query_android:any =
        {
            size:40,
			query: {
                query_string: {
                    query: "type:android",
                    type: "phrase"
                  }
				}
			}
        const result_android = await SearchAccess(query_android);

        let query_cash:any =
        {
            size:40,
			query: {
                query_string: {
                    query: "type:cash",
                    type: "phrase"
                  }
				}
			}
        const result_cash = await SearchAccess(query_cash);

        return res.status(200).json({ data_ios: result_ios,data_android:result_android,data_cash:result_cash })
    
}