import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import {SearchNews} from "../../lib"

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=10, stale-while-revalidate=59'
    )
    let string_query=""
    let title = req.body.title
    title=title.replace("/","")
    title=title.replace(":","")
    title=title.replace(",","")
    title.split(" ").map((value:any,index:any)=>{
        if(index==0)
        {
            string_query = value
        }
        else
        {
            string_query = string_query+ " OR "+ value
        }
    })
    
        let query:any =
        {
            size:11,
			query: {
                query_string: {
                    query: string_query,
                  }
				}
			}
        const result = await SearchNews(query);
        return res.status(200).json({ data: result.slice(1,11) })
    
}
