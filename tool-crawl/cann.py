from datetime import datetime
from itertools import count
from unidecode import unidecode
import time
import hashlib
from underthesea import word_tokenize

class CANN:
    def __init__(self):
        print("date")

    def _start_cand(self,browser,es):
        now = datetime.now()
        date_time_str = now.strftime("%d/%m/%Y")
        #date_time_str = "16/09/2022"
        _date = now.strftime("%Y-%m-%d")
        #_date = "2022-09-16"
        browser.get("https://cand.com.vn/phap-luat/")
        time.sleep(2)
        for j in range(4000):
            browser.execute_script("window.scrollTo(0, window.scrollY + 1)")
        list_url=[]
        list_img =[]
        for element in browser.find_element_by_class_name("boxlist-list").find_elements_by_class_name("box-img"):
            url = element.find_element_by_tag_name("a").get_attribute("href")
            image = element.find_element_by_tag_name("img").get_attribute("src").replace("600x600","320x320").replace("800x800","320x320")
            list_url.append(url)
            list_img.append(image)
        count=-1
        for url in list_url:
            count=count+1
            browser.get(url)
            body = browser.find_element_by_class_name("entry-content")
            _date_text = body.find_element_by_class_name("box-date").text
            index_date = _date_text.find(date_time_str)
            if index_date <0:
                continue
            time.sleep(5)
            for j in range(1500):
                browser.execute_script("window.scrollTo(0, window.scrollY + 2)")
            body = browser.find_element_by_class_name("entry-content")
            _date_text = body.find_element_by_class_name("box-date").text
            index_date = _date_text.find(date_time_str)
            
            lt_image=[]
            for element in body.find_elements_by_class_name("light-img"):
                try:
                    lt_image.append(element.find_element_by_tag_name("img").get_attribute("src"))
                except:
                    continue
            data={
                    "id":hashlib.md5(url.encode("utf-8")).hexdigest(),
                    "date":_date +"T"+_date_text[index_date+12:index_date+17]+":00",
                    "meta_title":body.find_element_by_class_name("box-title-detail").text,
                    "meta_img":list_img[count],
                    "meta_content":body.find_element_by_class_name("box-des-detail").find_element_by_tag_name("p").text,
                    "list_img":lt_image,
                    "content":body.find_element_by_class_name("detail-content-body").text,
                    "key_word":"",
                    "root_link":url,
                    "avata":"https://img.cand.com.vn/Content/images/logo.png",
                    "link":url.replace("https://cand.com.vn/Ban-tin-113/","").replace("https://cand.com.vn/Ban-tin-113/-","").replace("/","")
                }
            print(data['date'],data['id'])
            res = es.index(index='data_news', doc_type="_doc", id=data['id'], body=data)
            time.sleep(2)
            print(res)
            