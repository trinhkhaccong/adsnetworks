from datetime import datetime
from unidecode import unidecode
import time
import hashlib
from underthesea import word_tokenize

class NhanDan:
    def __init__(self):
        print("date")

    def _start_nhandan(self,browser,es):
        now = datetime.now()
        date_time_str = now.strftime("%d/%m/%Y")
        _date = now.strftime("%Y-%m-%d")
        browser.get("https://nhandan.vn/tin-moi.html")
        for j in range(3000):
            browser.execute_script("window.scrollTo(0, window.scrollY + 3)")
        for i in range(4):
            try:
                browser.find_element_by_class_name("see-more").click()
                for j in range(3000):
                    browser.execute_script("window.scrollTo(0, window.scrollY + 3)")
            except:
                print("err")
        _list=[]
        for element in browser.find_element_by_class_name("main-content").find_elements_by_class_name("story"):
            text_date = element.find_element_by_class_name("story__meta").text
            index_date = text_date.find(date_time_str)
            if index_date<0:
                continue
            root_link=element.find_element_by_class_name("story__heading").find_element_by_tag_name("a").get_attribute("href")
            try:
                data={
                    "id":hashlib.md5(root_link.encode("utf-8")).hexdigest(),
                    "date":_date +"T"+text_date[index_date+11:index_date+16]+":00",
                    "meta_title":element.find_element_by_class_name("story__heading").text,
                    "meta_img":element.find_element_by_tag_name("img").get_attribute("src"),
                    "meta_content":element.find_element_by_class_name("story__summary").text,
                    "list_img":[],
                    "content":"",
                    "key_word":"",
                    "root_link":element.find_element_by_class_name("story__heading").find_element_by_tag_name("a").get_attribute("href"),
                    "avata":"https://static-cms-nhandan.zadn.vn/web/styles/img/logo-nhandan_ver3.png",
                    "link":root_link.replace(".html","").replace("https://nhandan.vn/","")
                }
            except:
                continue
            _list.append(data)

        for object in _list:
            browser.get(object['root_link'])
            for j in range(500):
                browser.execute_script("window.scrollTo(0, window.scrollY + 3)")
            try:
                body = browser.find_element_by_class_name("main-col")
            except:
                continue
            object['content'] = body.find_element_by_class_name("article__body").text
            list_img=[]
            for element in body.find_elements_by_tag_name("img"):
                list_img.append(element.get_attribute("src"))
            object['list_img']=list_img
            res = es.index(index='data_news', doc_type="_doc", id=object['id'], body=object)
            time.sleep(2)
            print(res)
            