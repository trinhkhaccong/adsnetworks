from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from nhandan import NhanDan
from cann import CANN
from elasticsearch import Elasticsearch
es = Elasticsearch("185.219.132.66:9200")
if __name__ == "__main__":
    
    options = Options()
    options.add_argument('--disable-notifications')
    browser = webdriver.Chrome(chrome_options=options)
    browser.maximize_window()
    CANN()._start_cand(browser,es)
    NhanDan()._start_nhandan(browser,es)
        